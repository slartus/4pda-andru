package org.softeg.slartus.forpdaplus_andru.listtemplates;/*
 * Created by slinkin on 20.03.14.
 */

import androidx.fragment.app.Fragment;

import org.softeg.slartus.forpdaplus_andru.listfragments.TopicsHistoryListFragment;

public class TopicsHistoryBrickInfo extends BrickInfo {
    @Override
    public String getTitle() {
        return "Посещенные темы";
    }

    @Override
    public String getName() {
        return "TopicsHistory";
    }

    @Override
    public Fragment createFragment() {
        return new TopicsHistoryListFragment().setBrickInfo(this);
    }
}
