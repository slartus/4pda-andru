package org.softeg.slartus.forpdaplus_andru.browser;

import android.app.Activity;
import android.content.Context;

import org.softeg.slartus.forpdaplus_andru.classes.AdvWebView;


/*
 * Created by slinkin on 02.10.2014.
 */
public interface IWebViewClientListener {

    Activity getActivity();
    Context getContext();

    AdvWebView getWebView();

    void onPageStarted();

    void onPageFinished();
}
