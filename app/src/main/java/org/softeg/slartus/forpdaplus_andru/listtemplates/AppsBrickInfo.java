package org.softeg.slartus.forpdaplus_andru.listtemplates;

import androidx.fragment.app.Fragment;

import org.softeg.slartus.forpdaplus_andru.listfragments.AppsListFragment;

/**
 * Created by slinkin on 20.02.14.
 */
public class AppsBrickInfo extends BrickInfo {
    @Override
    public String getTitle() {
        return "Приложения";
    }

    @Override
    public String getName() {
        return "Applications";
    }

    @Override
    public Fragment createFragment() {
        return new AppsListFragment().setBrickInfo(this);
    }
}
