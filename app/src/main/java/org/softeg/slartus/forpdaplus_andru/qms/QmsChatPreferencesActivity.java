package org.softeg.slartus.forpdaplus_andru.qms;

import android.os.Bundle;

import org.softeg.slartus.forpdaplus_andru.R;
import org.softeg.slartus.forpdaplus_andru.prefs.BasePreferencesActivity;

/**
 * User: slinkin
 * Date: 18.06.12
 * Time: 14:55
 */
public class QmsChatPreferencesActivity extends BasePreferencesActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.qms_chat_prefs);
    }
}
