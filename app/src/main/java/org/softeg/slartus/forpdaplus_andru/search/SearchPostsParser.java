package org.softeg.slartus.forpdaplus_andru.search;

import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.softeg.slartus.forpdacommon.Http;
import org.softeg.slartus.forpdacommon.NotReportException;
import org.softeg.slartus.forpdaplus_andru.App;
import org.softeg.slartus.forpdaplus_andru.classes.Exceptions.MessageInfoException;
import org.softeg.slartus.forpdaplus_andru.classes.HtmlBuilder;
import org.softeg.slartus.forpdaplus_andru.classes.Post;
import org.softeg.slartus.forpdaplus_andru.classes.TopicBodyBuilder;
import org.softeg.slartus.forpdaplus_andru.classes.common.Functions;
import org.softeg.slartus.forpdaplus_andru.emotic.Smiles;

import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by IntelliJ IDEA.
 * User: slinkin
 * Date: 22.10.12
 * Time: 12:54
 * To change this template use File | Settings | File Templates.
 */
public class SearchPostsParser extends HtmlBuilder {
    private boolean m_SpoilerByButton = false;
    private Hashtable<String, String> m_EmoticsDict;

    public SearchPostsParser() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(App.getContext());
        m_SpoilerByButton = prefs.getBoolean("theme.SpoilerByButton", false);
        m_EmoticsDict = Smiles.getSmilesDict();
    }

    public SearchResult searchResult;

    public String parse(String body) throws MessageInfoException, NotReportException {
        return parse(body,"form#se-mod-frm>div.cat_name, div[data-post]");
    }
    public String parse(String body, String postsSelector) throws MessageInfoException, NotReportException {

        searchResult = createSearchResult(body);
        body = body.replace("height:300px;", "max-height:300px;");

        beginHtml("Результаты поиска");

        beginTopic(searchResult);

        m_Body.append("<div class=\"search-results\">");
        Document doc = Jsoup.parse(body, "https://"+Http.Host+"");
        Elements postsElements = doc.select(postsSelector);
        int count = postsElements.size();
        if (count > 1)
            for (int i = 0; i < count; i += 2) {
                m_Body.append(parsePost(postsElements.get(i), postsElements.get(i + 1)));
            }

        m_Body.append("</div>");
        endTopic(searchResult);
        return m_Body.toString();
    }

    private SearchResult createSearchResult(String page) {


        final Pattern pagesCountPattern = Pattern.compile("var pages = parseInt\\((\\d+)\\);", Pattern.CASE_INSENSITIVE);
        // https://4pda.ru/forum/index.php?act=search&source=all&result=posts&sort=rel&subforums=1&query=pda&forums=281&st=90
        final Pattern lastPageStartPattern = Pattern.compile("(https?://"+ Http.HostPattern+")?/forum/index.php\\?act=Search[^\"]*?st=(\\d+)", Pattern.CASE_INSENSITIVE);
        final Pattern currentPagePattern = Pattern.compile("<span class=\"pagecurrent\">(\\d+)</span>", Pattern.CASE_INSENSITIVE);

        SearchResult searchResult = new SearchResult();

        Matcher m = pagesCountPattern.matcher(page);
        if (m.find()) {
            searchResult.setPagesCount(m.group(1));
        }

        m = lastPageStartPattern.matcher(page);
        while (m.find()) {
            searchResult.setLastPageStartCount(m.group(2));
        }

        m = currentPagePattern.matcher(page);
        if (m.find()) {
            searchResult.setCurrentPage(m.group(1));
        } else
            searchResult.setCurrentPage("1");
        return searchResult;
    }

    private void beginTopic(SearchResult searchResult) {
        beginBody("search");

        if (searchResult.getPagesCount() > 1) {
            TopicBodyBuilder.addButtons(m_Body, searchResult.getCurrentPage(), searchResult.getPagesCount(),
                    Functions.isWebviewAllowJavascriptInterface(App.getInstance()), true, true);
        }
        m_Body.append("<br/><br/>");

    }

    private void endTopic(SearchResult searchResult) {
        m_Body.append("<div id=\"entryEnd\"></div>\n");
        m_Body.append("<br/><br/>");
        if (searchResult.getPagesCount() > 1) {
            TopicBodyBuilder.addButtons(m_Body, searchResult.getCurrentPage(),
                    searchResult.getPagesCount(),
                    Functions.isWebviewAllowJavascriptInterface(App.getInstance()), true, false);
        }

        m_Body.append("<br/><br/>");

        m_Body.append("<br/><br/><br/><br/><br/><br/>\n");

        endBody();
        endHtml();
    }


    private String parsePost(Element titleElement, Element element) {
        Boolean isWebviewAllowJavascriptInterface = Functions.isWebviewAllowJavascriptInterface(App.getInstance());
        String topic = "";

        String user = "";
        String dateTime = "";

        String post = "";
        String postFooter = "";
        Element el = titleElement.select("a").first();
        if (el != null)
            topic = el.outerHtml();

        el = element.select("span.post_nick").first();
        if (el != null) {
            el = el.select("a[href~=showuser=\\d+]").first();
            if (el != null) {
                Uri uri = Uri.parse(el.attr("href"));
                String userId = uri.getQueryParameter("showuser");
                String userName = el.text();

                String userState = el.html() != null && el.html().contains("color=\"green\"") ? "post_nick_online_cli" : "post_nick_cli";

                user = "<span class=\"" + userState + "\"><a " + TopicBodyBuilder.getHtmlout(isWebviewAllowJavascriptInterface, "showUserMenu",
                        userId, userName) + " class=\"system_link\">" + userName + "</a></span>";

            }
        }
        el = element.select("span.post_date").first();
        if (el != null) {
            dateTime = el.text().substring(0, el.text().indexOf("|"));
        }
        el = element.select("div.post_body").first();
        if (el != null) {
            post = Post.modifyBody(el.html(), m_EmoticsDict, true).replace("<br /><br />--------------------<br />", "");
            if (m_SpoilerByButton) {
                String find = "(<div class='hidetop' style='cursor:pointer;' )" +
                        "(onclick=\"var _n=this.parentNode.getElementsByTagName\\('div'\\)\\[1\\];if\\(_n.style.display=='none'\\)\\{_n.style.display='';\\}else\\{_n.style.display='none';\\}\">)" +
                        "(Спойлер \\(\\+/-\\).*?</div>)" +
                        "(\\s*<div class='hidemain' style=\"display:none\">)";
                String replace = "$1>$3<input class='spoiler_button' type=\"button\" value=\"+\" onclick=\"toggleSpoilerVisibility\\(this\\)\"/>$4";
                post = post.replaceAll(find, replace);
            }
        }

        String POST_TEMPLATE = "<div class=\"between_messages\"></div>\n" +
                "<div class=\"topic_title_post\">%1s</div>\n" +
                "<div class=\"post_header\">\n" +
                "\t<table width=\"100%%\">\n" +
                "\t\t<tr><td>%2s</td>\n" +
                "\t<td><div align=\"right\"><span class=\"post_date_cli\">%3s</span></div></td>\n" +
                "</tr>\n" +
                "</table>" +
                "</div>" +
                "<div class=\"post_body emoticons\">%4s</div>";
        return String.format(POST_TEMPLATE, topic, user, dateTime, post, postFooter);


    }
}


