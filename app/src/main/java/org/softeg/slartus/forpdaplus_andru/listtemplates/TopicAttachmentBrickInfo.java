package org.softeg.slartus.forpdaplus_andru.listtemplates;/*
 * Created by slinkin on 05.05.2014.
 */

import androidx.fragment.app.Fragment;

import org.softeg.slartus.forpdaplus_andru.listfragments.TopicAttachmentListFragment;

public class TopicAttachmentBrickInfo extends BrickInfo {
    public static final String NAME = "TopicAttachment";

    @Override
    public String getTitle() {
        return "Вложения";
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Fragment createFragment() {
        return new TopicAttachmentListFragment().setBrickInfo(this);
    }
}
