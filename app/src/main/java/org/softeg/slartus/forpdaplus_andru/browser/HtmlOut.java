package org.softeg.slartus.forpdaplus_andru.browser;/*
 * Created by slinkin on 16.02.2016.
 */

import android.content.Context;
import androidx.fragment.app.FragmentActivity;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class HtmlOut {
    public static final String NAME = "HTMLOUT";

    private WeakReference<IHtmlOutListener> mControl;

    protected IHtmlOutListener getControl(){
        return mControl.get();
    }
    public HtmlOut(IHtmlOutListener control) {

        mControl =new WeakReference<>( control);
    }


    protected Context getContext() {
        return mControl.get().getContext();
    }

    protected FragmentActivity getActivity() {
        return mControl.get().getActivity();
    }

    @JavascriptInterface
    public void showMessage(final String message) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT).show();
            }
        });
    }
}
