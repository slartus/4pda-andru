package org.softeg.slartus.forpdaplus_andru.listtemplates;/*
 * Created by slinkin on 10.04.2014.
 */

import androidx.fragment.app.Fragment;

import org.softeg.slartus.forpdaplus_andru.listfragments.users.LeadersListFragment;

public class LeadsBrickInfo extends BrickInfo {
    @Override
    public String getTitle() {
        return "Администрация";
    }

    @Override
    public String getName() {
        return "Leads";
    }

    @Override
    public Fragment createFragment() {
        return new LeadersListFragment().setBrickInfo(this);
    }
}
