package org.softeg.slartus.forpdaplus_andru;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.AsyncTaskLoader;
import androidx.loader.content.Loader;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;

import org.softeg.slartus.forpdaapi.classes.WebPageInfo;
import org.softeg.slartus.forpdaapi.devdb.DevDbApi;
import org.softeg.slartus.forpdacommon.Http;
import org.softeg.slartus.forpdaplus_andru.browser.AppWebChromeClient;
import org.softeg.slartus.forpdaplus_andru.browser.AppWebViewClient;
import org.softeg.slartus.forpdaplus_andru.browser.DeveloperInterface;
import org.softeg.slartus.forpdaplus_andru.browser.IWebViewClientListener;
import org.softeg.slartus.forpdaplus_andru.classes.AdvWebView;
import org.softeg.slartus.forpdaplus_andru.classes.HtmlBuilder;
import org.softeg.slartus.forpdaplus_andru.classes.common.ExtUrl;
import org.softeg.slartus.forpdaplus_andru.common.AppLog;

/**
 * User: slinkin
 * Date: 25.11.11
 * Time: 13:05
 */
public class DevDbDeviceActivity extends BaseFragmentActivity
        implements LoaderManager.LoaderCallbacks<WebPageInfo>, DeveloperInterface.IBrowserDeveloper,
        IWebViewClientListener {
    private AdvWebView mWebView;
    private static final String DEVICE_ID_KEY = "DeviceId";
    private static final String DEVICE_TITLE_KEY = "DEVICE_TITLE_KEY";

    private String m_DeviceId;
    private DeveloperInterface m_DeveloperInterface;
    private String m_DeviceTitle;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_devdb_device_);

        mSwipeRefreshLayout = createSwipeRefreshLayout();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        mWebView = (AdvWebView) findViewById(R.id.webView);
        mWebView.setWebChromeClient(new AppWebChromeClient());
        if (!App.getInstance().isWhiteTheme()) {
            mWebView.setBackgroundColor(App.getInstance().getThemeStyleWebViewBackground());
            mWebView.loadData("<html><head></head><body bgcolor=" + App.getInstance().getCurrentThemeName()
                    + "></body></html>", "text/html", "UTF-8");
        }
        registerForContextMenu(mWebView);

        m_DeveloperInterface = new DeveloperInterface(this, "DevDbDevice");
        mWebView.addJavascriptInterface(m_DeveloperInterface, DeveloperInterface.NAME);
        mWebView.getSettings().setJavaScriptEnabled(true);
        if (savedInstanceState != null) {
            m_DeviceId = savedInstanceState.getString(DEVICE_ID_KEY);
            m_DeviceTitle = savedInstanceState.getString(DEVICE_TITLE_KEY);
        } else {
            m_DeviceId = getIntent().getStringExtra(DEVICE_ID_KEY);
            m_DeviceTitle = getIntent().getStringExtra(DEVICE_TITLE_KEY);
        }
        if (!TextUtils.isEmpty(m_DeviceTitle))
            setTitle(m_DeviceTitle);
        loadPage();
    }

    protected SwipeRefreshLayout createSwipeRefreshLayout() {
        final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.ptr_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadPage();
            }
        });
        //swipeRefreshLayout.setColorSchemeResources(MyApp.getInstance().getMainAccentColor());
        return swipeRefreshLayout;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {

            onBackPressed();
            return true;
        }

        return true;
    }


    @Override
    protected void onStart() {
        super.onStart();
        mWebView.setWebViewClient(new AppWebViewClient(this));
    }

    @Override
    protected void onStop() {
        super.onStop();
        mWebView.setWebViewClient(new AppWebViewClient(this));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(DEVICE_ID_KEY, m_DeviceId);
    }

    public static void showActivity(Context context, String deviceId, String deviceTitle) {
        //   ExtUrl.showInBrowser(context, deviceId);
        Intent intent = new Intent(context, DevDbDeviceActivity.class);
        intent.putExtra(DEVICE_ID_KEY, deviceId);
        intent.putExtra(DEVICE_TITLE_KEY, deviceTitle);
        context.startActivity(intent);
    }

    public void loadPage() {
        Bundle args = new Bundle();
        args.putString(DEVICE_ID_KEY, m_DeviceId);

        setLoading(true);

        if (getSupportLoaderManager().getLoader(PageLoader.ID) != null)
            getSupportLoaderManager().restartLoader(PageLoader.ID, args, this);
        else
            getSupportLoaderManager().initLoader(PageLoader.ID, args, this);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {
        final WebView.HitTestResult hitTestResult = mWebView.getHitTestResult();
        switch (hitTestResult.getType()) {
            case WebView.HitTestResult.UNKNOWN_TYPE:
            case WebView.HitTestResult.EDIT_TEXT_TYPE:
                break;
            default: {
                showLinkMenu(hitTestResult.getExtra());
            }
        }
    }

    private void showLinkMenu(String link) {
        if (TextUtils.isEmpty(link) || link.contains("HTMLOUT.ru")
                || link.equals("#")
                || link.startsWith("file:///")) return;

        ExtUrl.showSelectActionDialog(new Handler(), this, m_DeviceTitle, "", link, null,
                null, null, null, null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("Ссылка...")
                .setIcon(R.drawable.ic_menu_goto)
                .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        ExtUrl.showSelectActionDialog(new Handler(), DevDbDeviceActivity.this, m_DeviceId);
                        return true;
                    }
                });

        menu.add("Закрыть")
                .setIcon(R.drawable.ic_menu_close_clear_cancel)
                .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {

                        finish();
                        return true;
                    }
                });

        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == DeveloperInterface.FILECHOOSER_RESULTCODE) {
            m_DeveloperInterface.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void saveHtml() {
        mWebView.evalJs("window." + DeveloperInterface.NAME + ".saveHtml('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
    }


    @Override
    public Loader<WebPageInfo> onCreateLoader(int id, Bundle args) {

        setLoading(true);

        return new PageLoader(this, args.getString(DEVICE_ID_KEY));
    }

    private void setLoading(final boolean loading) {
        try {
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(loading);
                }
            });
        } catch (Throwable ignore) {
            android.util.Log.e("TAG", ignore.toString());
        }
    }

    @Override
    public void onLoadFinished(Loader<WebPageInfo> loader, WebPageInfo data) {
        if (data != null && data.getError() != null) {
            AppLog.e(this, data.getError());
            setSupportProgressBarIndeterminateVisibility(false);
        } else if (data != null) {
            mWebView.loadDataWithBaseURL("https://"+ Http.Host+"", data.getPage(), "text/html", "UTF-8", null);
            if (!TextUtils.isEmpty(data.getTitle()))
                setTitle(data.getTitle());
        }

        setLoading(false);
    }

    @Override
    public void onLoaderReset(Loader<WebPageInfo> loader) {

    }

    @Override
    public FragmentActivity getActivity() {
        return this;
    }

    @Override
    public AdvWebView getWebView() {
        return mWebView;
    }

    @Override
    public void onPageStarted() {
        setSupportProgressBarIndeterminateVisibility(true);
    }

    @Override
    public void onPageFinished() {
        setSupportProgressBarIndeterminateVisibility(false);
    }


    public static class PageLoader extends AsyncTaskLoader<WebPageInfo> {

        public static final int ID = 111;
        WebPageInfo mApps;
        private String mUrl;

        public PageLoader(Context context, String url) {
            super(context);
            mUrl = url;
        }

        @Override
        public WebPageInfo loadInBackground() {
            WebPageInfo data = new WebPageInfo();
            try {
                data = DevDbApi.getDevice(Client.getInstance(), mUrl);
                DevDbHtmlBuilder htmlBuilder = new DevDbHtmlBuilder();
                htmlBuilder.beginHtml(data.getTitle());
                htmlBuilder.beginBody("devdb-device");
                htmlBuilder.append("<div class=\"device-frame\">");
                htmlBuilder.append(data.getPage());
                htmlBuilder.append("</div>");
                htmlBuilder.endBody();
                htmlBuilder.endHtml();
                data.setPage(htmlBuilder.getHtml().toString());
            } catch (Throwable e) {

                data.setError(e);
            }

            return data;

        }

        @Override
        public void deliverResult(WebPageInfo apps) {
            mApps = apps;

            if (isStarted()) {
                super.deliverResult(apps);
            }
        }

        @Override
        protected void onStartLoading() {

            if (mApps != null) {
                // If we currently have a result available, deliver it
                // immediately.
                deliverResult(mApps);
            }

            if (takeContentChanged() || mApps == null) {
                // If the data has changed since the last time it was loaded
                // or is not currently available, start a load.
                forceLoad();
            }
        }


        @Override
        protected void onStopLoading() {
            // Attempt to cancel the current load task if possible.
            cancelLoad();
        }

        @Override
        protected void onReset() {
            super.onReset();

            // Ensure the loader is stopped
            onStopLoading();
        }

        private class DevDbHtmlBuilder extends HtmlBuilder {



            @Override
            protected String getStyle() {
                return "/android_asset/forum/css/" + (App.getInstance().isWhiteTheme() ? "devdb_light.css" : "devdb_dark.css");

            }

            @Override
            public void addScripts() {
                super.addScripts();
                m_Body.append("<script type=\"text/javascript\" src=\"file:///android_asset/forum/js/devdb.js\"></script>\n");
            }
        }
    }


}
