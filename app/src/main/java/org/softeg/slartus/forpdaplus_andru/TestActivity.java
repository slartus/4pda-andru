package org.softeg.slartus.forpdaplus_andru;/*
 * Created by slinkin on 14.05.2014.
 */

import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import android.webkit.WebView;

public class TestActivity extends FragmentActivity {

    protected boolean isTransluent() {
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.test);

        ((WebView)findViewById(R.id.wvBody)).loadUrl("http://stackoverflow.com/");
    }
}
