package org.softeg.slartus.forpdaplus_andru.listtemplates;

import androidx.fragment.app.Fragment;

import org.softeg.slartus.forpdaplus_andru.listfragments.DevDbCatalogFragment;

/*
 * Created by slartus on 06.03.14.
 */
public class DevDbCatalogBrickInfo extends BrickInfo {
    @Override
    public String getTitle() {
        return "DevDB";
    }

    @Override
    public String getName() {
        return "devdb_catalog";
    }

    @Override
    public Fragment createFragment() {
        return new DevDbCatalogFragment().setBrickInfo(this);
    }
}
