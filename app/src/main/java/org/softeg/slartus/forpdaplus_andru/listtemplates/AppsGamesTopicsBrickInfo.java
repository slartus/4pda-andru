package org.softeg.slartus.forpdaplus_andru.listtemplates;/*
 * Created by slinkin on 17.03.14.
 */

import androidx.fragment.app.Fragment;

import org.softeg.slartus.forpdaplus_andru.listfragments.AppsGamesTopicsListFragment;

public class AppsGamesTopicsBrickInfo extends BrickInfo {

    @Override
    public String getTitle() {
        return "Каталог@темы";
    }

    @Override
    public String getName() {
        return "AppsGamesTopics";
    }

    @Override
    public Fragment createFragment() {
        return new AppsGamesTopicsListFragment().setBrickInfo(this);
    }
}
