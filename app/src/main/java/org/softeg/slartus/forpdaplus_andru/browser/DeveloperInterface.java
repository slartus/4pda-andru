package org.softeg.slartus.forpdaplus_andru.browser;/*
 * Created by slinkin on 19.11.2015.
 */

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import androidx.fragment.app.FragmentActivity;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import org.softeg.slartus.forpdacommon.FileUtils;
import org.softeg.slartus.forpdaplus_andru.App;
import org.softeg.slartus.forpdaplus_andru.classes.AdvWebView;
import org.softeg.slartus.forpdaplus_andru.common.AppLog;

import java.io.File;
import java.io.FileWriter;

public class DeveloperInterface {

    public interface IBrowserDeveloper {
        FragmentActivity getActivity();
        Context getContext();
        AdvWebView getWebView();
    }

    public static final String NAME = "DEVOUT";
    private FragmentActivity activity;
    private IBrowserDeveloper browser;
    private String title;

    private Context getContext(){
        return browser.getContext();
    }

    public DeveloperInterface(IBrowserDeveloper browser, String title) {
        this.browser = browser;
        this.title = title;
        this.activity = browser.getActivity();

    }

    private FragmentActivity getActivity() {
        return activity;
    }

    public final static int FILECHOOSER_RESULTCODE = App.getInstance().getUniqueIntValue();

    @JavascriptInterface
    public void showChooseCssDialog() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    intent.setType("file/*");

                    // intent.setDataAndType(Uri.parse("file://" + lastSelectDirPath), "file/*");
                    getActivity().startActivityForResult(intent, FILECHOOSER_RESULTCODE);

                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(getActivity(), "Ни одно приложение не установлено для выбора файла!", Toast.LENGTH_LONG).show();
                } catch (Exception ex) {
                    AppLog.e(getActivity(), ex);
                }
            }
        });
    }

    @JavascriptInterface
    public void saveHtml(final String html) {
        saveHtml(getActivity(), html, "Topic");
    }


    public void onActivityResult(int requestCode, int resultCode,
                                 Intent data) {

        if (resultCode == Activity.RESULT_OK && requestCode == FILECHOOSER_RESULTCODE) {
            if (data.getData() == null)
                return;

            try {
                String attachFilePath = FileUtils.getRealPathFromURI(App.getInstance(), data.getData());
                String cssData = FileUtils.readFileText(attachFilePath)
                        .replace("\\", "\\\\")
                        .replace("'", "\\'").replace("\"", "\\\"").replace("\n", "\\n").replace("\r", "");

                browser.getWebView().evalJs("window['HtmlInParseLessContent']('" + cssData + "');");
            } catch (Exception e) {
                AppLog.e(e);
            }
        }
    }

    public void saveHtml(final Activity activity, final String html, final String defaultFileName) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    String state = Environment.getExternalStorageState();
                    if (!Environment.MEDIA_MOUNTED.equals(state)) {
                        Toast.makeText(getContext(), "Внешнее хранилище недоступно!", Toast.LENGTH_SHORT).show();
                        return;
                    }


                    File file = new File(App.getInstance().getAppExternalFolderPath(), title+".txt");
                    FileWriter out = new FileWriter(file);
                    out.write(html);
                    out.close();
                    Uri uri = Uri.fromFile(file);

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(uri, "text/plain");
                    getContext().startActivity(intent);
                } catch (Exception e) {
                    AppLog.e(getContext(), e);
                }
            }
        });

    }
}
