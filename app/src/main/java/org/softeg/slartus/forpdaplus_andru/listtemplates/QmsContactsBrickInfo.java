package org.softeg.slartus.forpdaplus_andru.listtemplates;/*
 * Created by slinkin on 07.05.2014.
 */

import androidx.fragment.app.Fragment;

import org.softeg.slartus.forpdaplus_andru.qms.QmsContactsListFragment;

public class QmsContactsBrickInfo extends BrickInfo {
    public static final String NAME = "QmsContacts";

    @Override
    public String getTitle() {
        return "Контакты";
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Fragment createFragment() {
        return new QmsContactsListFragment().setBrickInfo(this);
    }
}
