package org.softeg.slartus.forpdaplus_andru.listtemplates;/*
 * Created by slinkin on 21.03.14.
 */

import androidx.fragment.app.Fragment;

import org.softeg.slartus.forpdaplus_andru.listfragments.NotesListFragment;

public class NotesBrickInfo extends BrickInfo {
    @Override
    public String getTitle() {
        return "Заметки";
    }

    @Override
    public String getName() {
        return "Notes";
    }

    @Override
    public Fragment createFragment() {
        return new NotesListFragment().setBrickInfo(this);
    }
}
