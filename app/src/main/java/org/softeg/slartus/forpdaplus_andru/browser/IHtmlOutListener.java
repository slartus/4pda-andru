package org.softeg.slartus.forpdaplus_andru.browser;/*
 * Created by slinkin on 16.02.2016.
 */

import android.content.Context;
import androidx.fragment.app.FragmentActivity;

public interface IHtmlOutListener {

    Context getContext();

    FragmentActivity getActivity();
}
