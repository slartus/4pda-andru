package org.softeg.slartus.forpdaplus_andru.listtemplates;/*
 * Created by slinkin on 18.03.14.
 */

import androidx.fragment.app.Fragment;

import org.softeg.slartus.forpdaplus_andru.listfragments.DigestCatalogFragment;

public class DigestCatalogBrickInfo extends BrickInfo {
    @Override
    public String getTitle() {
        return "Дайджест игр и приложений";
    }

    @Override
    public String getName() {
        return "DigestCatalog";
    }

    @Override
    public Fragment createFragment() {
        return new DigestCatalogFragment().setBrickInfo(this);
    }
}
