package org.softeg.slartus.forpdaplus_andru.listtemplates;/*
 * Created by slinkin on 17.03.14.
 */

import androidx.fragment.app.Fragment;

import org.softeg.slartus.forpdaplus_andru.listfragments.AppsGamesCatalogFragment;

public class AppsGamesCatalogBrickInfo extends BrickInfo {
    @Override
    public String getTitle() {
        return "Каталог игр и приложений";
    }

    @Override
    public String getName() {
        return "AppsGamesCatalog";
    }

    @Override
    public Fragment createFragment() {
        return new AppsGamesCatalogFragment().setBrickInfo(this);
    }
}
