package org.softeg.slartus.forpdaplus_andru.classes;

/**
 * Created by slinkin on 16.01.14.
 */
public interface IListItem {
    CharSequence getTitle();

    CharSequence getId();
}
