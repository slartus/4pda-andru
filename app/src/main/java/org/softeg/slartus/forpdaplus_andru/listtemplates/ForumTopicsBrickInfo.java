package org.softeg.slartus.forpdaplus_andru.listtemplates;

import androidx.fragment.app.Fragment;

import org.softeg.slartus.forpdaplus_andru.listfragments.ForumTopicsListFragment;

/**
 * Created by slinkin on 03.03.14.
 */
public class ForumTopicsBrickInfo extends BrickInfo {
    @Override
    public String getTitle() {
        return "Темы форума";
    }

    @Override
    public String getName() {
        return "ForumTopics";
    }

    @Override
    public Fragment createFragment() {
        return new ForumTopicsListFragment().setBrickInfo(this);
    }


}
