package org.softeg.slartus.forpdaplus_andru.listtemplates;/*
 * Created by slinkin on 13.03.14.
 */

import androidx.fragment.app.Fragment;

import org.softeg.slartus.forpdaplus_andru.listfragments.DevDbModelsFragment;

public class DevDbModelsBrickInfo extends BrickInfo {
    @Override
    public String getTitle() {
        return "DevDb.ru";
    }

    @Override
    public String getName() {
        return "devdb_models";
    }

    @Override
    public Fragment createFragment() {
        return new DevDbModelsFragment().setBrickInfo(this);
    }
}

