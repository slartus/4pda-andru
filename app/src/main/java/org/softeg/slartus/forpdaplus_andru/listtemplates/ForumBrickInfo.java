package org.softeg.slartus.forpdaplus_andru.listtemplates;

import androidx.fragment.app.Fragment;

import org.softeg.slartus.forpdaplus_andru.listfragments.next.ForumFragment;

/**
 * Created by slinkin on 21.02.14.
 */
public class ForumBrickInfo extends BrickInfo {
    public static final String NAME = "Forum";

    @Override
    public String getTitle() {
        return "Форум";
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Fragment createFragment() {
        return new ForumFragment().setBrickInfo(this);
    }
}
