package org.softeg.slartus.forpdaplus_andru.browser;/*
 * Created by slinkin on 19.11.2015.
 */

import android.annotation.TargetApi;
import android.os.Build;
import android.util.Log;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

public class AppWebChromeClient extends WebChromeClient {

    public void onConsoleMessage(String message, int lineNumber, String sourceID) {
        Log.d("AppWebChromeClient", message + " -- From line "
                + lineNumber + " of "
                + sourceID);
    }

    @TargetApi(Build.VERSION_CODES.FROYO)
    public boolean onConsoleMessage(ConsoleMessage cm) {
        Log.d("AppWebChromeClient", cm.message() + " -- From line "
                + cm.lineNumber() + " of "
                + cm.sourceId() );
        return true;
    }

    @Override
    public void onReceivedTouchIconUrl(WebView view, String url, boolean precomposed) {
        super.onReceivedTouchIconUrl(view, url, precomposed);
    }
}
