package org.softeg.slartus.forpdaplus_andru.listtemplates;/*
 * Created by slinkin on 07.04.2014.
 */

import androidx.fragment.app.Fragment;

public class ForumSearchBrickInfo extends BrickInfo {
    @Override
    public String getTitle() {
        return "Поиск по форуму";
    }

    @Override
    public String getName() {
        return "Search";
    }

    @Override
    public Fragment createFragment() {
        return null;
    }
}
