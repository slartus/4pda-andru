package org.softeg.slartus.forpdaplus_andru.listtemplates;

import androidx.fragment.app.Fragment;

import org.softeg.slartus.forpdaplus_andru.listfragments.news.NewsNavigationFragment;

import java.io.Serializable;

/*
 * Created by slinkin on 20.02.14.
 */
public class NewsPagerBrickInfo extends BrickInfo implements Serializable{
    @Override
    public String getTitle() {
        return "Новости";
    }

    @Override
    public String getName() {
        return "News_Pages";
    }

    @Override
    public Fragment createFragment() {
        return NewsNavigationFragment.createInstance(this);
    }
}
