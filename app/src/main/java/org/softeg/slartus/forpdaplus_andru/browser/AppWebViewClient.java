package org.softeg.slartus.forpdaplus_andru.browser;/*
 * Created by slinkin on 19.11.2015.
 */

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.dmitriy.tarasov.android.intents.IntentUtils;

import org.softeg.slartus.forpdaplus_andru.Client;
import org.softeg.slartus.forpdaplus_andru.IntentActivity;
import org.softeg.slartus.forpdaplus_andru.common.AppLog;

import java.lang.ref.WeakReference;

public class AppWebViewClient extends WebViewClient {
    private WeakReference<IWebViewClientListener> listener;

    public AppWebViewClient(IWebViewClientListener listener) {
        super();
        this.listener = new WeakReference<>(listener);
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);

        listener.get().onPageStarted();
        //ThemeActivity.this.setProgressBarIndeterminateVisibility(true);
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        view.clearHistory();
        listener.get().onPageFinished();
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, final String url) {
        try {
            if (url != null && url.startsWith("file:///android_asset/webkit/"))
                return true;
            if (IntentActivity.tryShowUrl(listener.get().getActivity(),new Handler(), url,true,false, Client.getInstance().getAuthKey())) {
                return true;
            }

            Intent intent = Intent.createChooser(IntentUtils.openLink(url), "");
            listener.get().getContext().startActivity(intent);
        } catch (Throwable ex) {
            AppLog.e(ex);
        }
        return true;
    }

    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        super.onReceivedError(view, errorCode, description, failingUrl);
        Log.d("AppWebViewClient", description);

    }

    @Override
    public void onLoadResource(WebView view, String url) {
        super.onLoadResource(view, url);
        Log.d("onLoadResource", url);
    }
}
