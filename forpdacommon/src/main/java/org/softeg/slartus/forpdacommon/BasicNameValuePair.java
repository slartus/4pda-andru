package org.softeg.slartus.forpdacommon;

/*
 * Created by slinkin on 28.11.2018.
 */
public class BasicNameValuePair extends NameValuePair {

    public BasicNameValuePair(String name, String value){
        _name=name;
        _value=value;
    }
}
