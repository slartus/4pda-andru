package org.softeg.slartus.forpdacommon;

/*
 * Created by slinkin on 28.11.2018.
 */
public abstract class NameValuePair {
    protected String _name;
    protected String _value;
}
