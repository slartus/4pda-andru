package org.softeg.slartus.forpdacommon;

/*
 * Created by slinkin on 28.11.2018.
 */
public abstract class Predicate<T> {
    public abstract boolean apply(T t);
}
