package org.softeg.slartus.forpdaapi.classes;

/*
 * Created by slinkin on 15.06.2015.
 */

/*
 * Created by slinkin on 15.06.2015.
 */
public class LoginForm {
    private Throwable error;
    private String captchaPath;
    private String captchaTime;
    private String captchaSig;
    private String session;

    public String getCaptchaPath() {
        return captchaPath;
    }

    public void setCapPath(String captchaPath) {
        this.captchaPath = captchaPath;
    }

    public String getCaptchaTime() {
        return captchaTime;
    }

    public void setCapTime(String captchaTime) {
        this.captchaTime = captchaTime;
    }

    public String getCaptchaSig() {
        return captchaSig;
    }

    public void setCapSig(String captchaSig) {
        this.captchaSig = captchaSig;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getSession() {
        return session;
    }
}
