package org.softeg.slartus.forpdaapi.devdb;

import android.net.Uri;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.softeg.slartus.forpdaapi.IHttpClient;
import org.softeg.slartus.forpdaapi.classes.WebPageInfo;
import org.softeg.slartus.forpdacommon.Http;
import org.softeg.slartus.forpdacommon.ShowInBrowserException;
import org.softeg.slartus.forpdacommon.UrlExtensions;

import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by slartus on 06.03.14.
 */
public class DevDbApi {

    @Retention(RetentionPolicy.SOURCE)

    public @interface DeviceType {
    }

    public static String getDeviceTypeTitle(@DeviceType String deviceType) {
        switch (deviceType.toLowerCase()) {
            case PHONES_TYPE:
                return "Телефоны";
            case PAD_TYPE:
                return "Планшеты";
            case EBOOK_TYPE:
                return "Электронные книги";
            default:
                return deviceType;
        }
    }

    public static String getDeviceTypePattern() {
        return PHONES_TYPE + "|" + PAD_TYPE + "|" + EBOOK_TYPE;
    }

    public static final String PHONES_TYPE = "phones";
    public static final String PAD_TYPE = "pad";
    public static final String EBOOK_TYPE = "ebook";

    public static ArrayList<DevCatalog> parseDevicesTypes(IHttpClient client) throws Throwable {
        String pageBody = client.performGet("https://devdb.ru");

        Pattern pattern = Pattern.compile("<a href=\"(https?://devdb.ru/\\w+)/\"><p><img src=\"([^\"]*)\" alt=\"([^\"]*)\"[^/>]*/><br /><br />([^<]*)</a></p>",
                Pattern.CASE_INSENSITIVE);
        Matcher m = pattern.matcher(pageBody);
        ArrayList<DevCatalog> res = new ArrayList<>();
        while (m.find()) {
            DevCatalog f = new DevCatalog(m.group(1), m.group(4));
            f.setDescription(m.group(3));
            f.setImageUrl(UrlExtensions.removeDoubleSplitters("https://devdb.ru/" + m.group(2)));
            f.setType(DevCatalog.DEVICE_TYPE);
            res.add(f);
        }
        res.add(new DevCatalog("https://devdb.ru/accessories/", "Акссесуары").setType(DevCatalog.DEVICE_TYPE));
        return res;
    }

    public static ArrayList<DevCatalog> getStandartDevicesTypes() {
        ArrayList<DevCatalog> res = new ArrayList<>();

        res.add(new DevCatalog(DevDbApi.PHONES_TYPE, DevDbApi.getDeviceTypeTitle(DevDbApi.PHONES_TYPE)));
        res.add(new DevCatalog(DevDbApi.PAD_TYPE, DevDbApi.getDeviceTypeTitle(DevDbApi.PAD_TYPE)));
        res.add(new DevCatalog(DevDbApi.EBOOK_TYPE, DevDbApi.getDeviceTypeTitle(DevDbApi.EBOOK_TYPE)));

        return res;
    }

    public static ArrayList<DevCatalog> parseBrands(IHttpClient client, String deviceTypeId) throws Throwable {
        String pageBody = client.performGet("https://"+Http.Host+"/devdb/" + deviceTypeId + "/all");

        Pattern pattern = Pattern.compile("<li><a href=\"https?://"+ Http.HostPattern+"/devdb/([^\"/]*/[^\"/#]*)(?:/all)?\">([^<>]*)</a>");
        Matcher m = pattern.matcher(pageBody);
        ArrayList<DevCatalog> res = new ArrayList<>();
        while (m.find()) {
            DevCatalog f = new DevCatalog(m.group(1), m.group(2));
            f.setType(DevCatalog.DEVICE_BRAND);
            res.add(f);
        }
        Collections.sort(res, new Comparator<DevCatalog>() {
            @Override
            public int compare(DevCatalog forum, DevCatalog t1) {
                return forum.getTitle().toString().compareTo(t1.getTitle().toString());
            }
        });

        return res;
    }

    public static ArrayList<DevModel> parseModels(IHttpClient client, String brandId) throws Throwable {

        ArrayList<DevModel> res = new ArrayList<>();
        String pageBody = client.performGet("https://"+Http.Host+"/devdb/" + brandId + "/all");

        Document doc = Jsoup.parse(pageBody);
        Elements con = doc.select("div.box-holder");

        for (int i = 0; i < con.size(); i++) {
            String link = con.get(i).select(".visual a").first().attr("href");
            String title = con.get(i).select(".visual a").first().attr("title");
            String image = con.get(i).select(".visual img").first().attr("src");

            DevModel model = new DevModel(link, title);
            model.setImgUrl(image);
            model.setDescription(con.get(i).select(".frame .specifications-list").first().text());

            res.add(model);
        }

        Collections.sort(res, new Comparator<DevModel>() {
            @Override
            public int compare(DevModel forum, DevModel t1) {
                return forum.getTitle().compareTo(t1.getTitle());
            }
        });
        return res;

    }

    public static Boolean isCatalogUrl(String url) {
        return Pattern
                .compile("https?://"+ Http.HostPattern+"/devdb/(" + DevDbApi.getDeviceTypePattern() + ")/([^\"/#]+?)/?$", Pattern.CASE_INSENSITIVE)
                .matcher(url).find();
    }

    public static Boolean isDevicesListUrl(String url) {
        return Pattern
                .compile("https?://"+Http.HostPattern+"/devdb/(" + DevDbApi.getDeviceTypePattern() + ")/?$", Pattern.CASE_INSENSITIVE)
                .matcher(url).find();
    }

    public static Boolean isDeviceUrl(String url) {
        return Pattern
                .compile("((?:https?://)?"+ Http.HostPattern+"/devdb/[^\"/_]+?_[^\"/#]+?)/?$", Pattern.CASE_INSENSITIVE)
                .matcher(url).find();
    }

    public static DevCatalog getCatalog(String url) throws ShowInBrowserException {
        Matcher m = Pattern.compile("devdb.ru", Pattern.CASE_INSENSITIVE).matcher(url);
        if (!m.find())
            throw new ShowInBrowserException("Не умею обрабаывать ссылки такого типа!", url);

        Uri uri = Uri.parse(url);
        DevCatalog root = new DevCatalog("-1", "DevDb.ru").setType(DevCatalog.ROOT);
        if (uri.getPathSegments() == null || uri.getPathSegments().size() <= 0)
            return root;

        String title = uri.getPathSegments().get(0);
        switch (uri.getPathSegments().get(0).toLowerCase()) {
            case "pda":
                title = "Коммуникаторы";
                break;
            case "phone":
                title = "Сотовые телефоны";
                break;
            case "pnd":
                title = "Навигаторы";
                break;
            case "ebook":
                title = "Электронные книги";
                break;
            case "pad":
                title = "Планшеты";
                break;
            case "accessories":
                title = "Акссесуары";
                break;
            default:
                return root;
        }
        DevCatalog devType = new DevCatalog("https://devdb.ru/" + uri.getPathSegments().get(0), title)
                .setType(DevCatalog.DEVICE_TYPE);
        devType.setParent(root);
        if (uri.getPathSegments().size() < 2) {
            return devType;
        }
        DevCatalog catalog = new DevCatalog(url, uri.getPathSegments().get(1));
        catalog.setType(DevCatalog.DEVICE_BRAND);
        catalog.setParent(devType);
        return catalog;

    }

    public static WebPageInfo getDevice(IHttpClient client, String deviceUrl) throws IOException {
        WebPageInfo pageInfo = new WebPageInfo();

        String pageBody = client.performGet(deviceUrl);

        Document doc = Jsoup.parse(pageBody);

        Elements elements=doc.select("ul.tab-control li>a");
        if(elements!=null)
            for(Element el:elements){
                el.attributes().put("onclick","javascript:clickA(this);");
            }

        StringBuilder sb = new StringBuilder();

        Element element = doc.select("div.head").first();
        if (element != null)
            sb.append(element.html());

        element = doc.select("div.tabset").first();
        if (element != null) {
            element.select("div.item-tools").remove();
            element.select("div.ads").remove();
            sb.append(element.html());
        }


//        element = doc.select("div.item-content").first();
//        if (element != null) {
//            element.select("div.ads").remove();
//            sb.append(element.html());
//        }
        pageInfo.setPage(sb.toString());
        return pageInfo;
    }

}
