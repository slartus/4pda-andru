package org.softeg.slartus.forpdaapi.search;/*
 * Created by slinkin on 29.04.2014.
 */

import android.net.Uri;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.softeg.slartus.forpdaapi.IHttpClient;
import org.softeg.slartus.forpdaapi.ListInfo;
import org.softeg.slartus.forpdaapi.Topic;
import org.softeg.slartus.forpdacommon.Functions;
import org.softeg.slartus.forpdacommon.Http;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SearchApi {
    /**
     * @param searchUrl - номер страницы из
     * @throws MalformedURLException
     */
    public static ArrayList<Topic> getSearchTopicsResult(IHttpClient client, String searchUrl, ListInfo listInfo)
            throws IOException {
        int st = 0;
        Matcher m = Pattern.compile("st=(\\d+)", Pattern.CASE_INSENSITIVE).matcher(searchUrl);
        if (m.find())
            st = Integer.parseInt(m.group(1));
        st += listInfo.getFrom();

        String body = client.performGet(searchUrl.replaceAll("st=\\d+", "") + "&st=" + st);



        return parse(body,listInfo);
    }

    public static  ArrayList<Topic> parse(String pageBody, ListInfo listInfo){
ArrayList<Topic> res=new ArrayList<>();
        Document doc = Jsoup.parse(pageBody, "https://"+ Http.Host+"");
        Elements postsElements = doc.select("div[data-topic]");
        Pattern descriptionPattern = Pattern.compile("^(.*?)(?:\\n|форум|автор)", Pattern.CASE_INSENSITIVE);

        String today = Functions.getToday();
        String yesterday = Functions.getYesterToday();
        for (Element element : postsElements) {
            Element el = element.select("a[href~=showtopic=\\d+]").first();

            Topic topic;
            if (el != null) {
                Uri uri = Uri.parse(el.attr("href"));
                String id = uri.getQueryParameter("showtopic");
                String title = el.text();
                topic = new Topic(id, title);
            } else
                continue;

            el = element.select("span.modifier").first();
            int state = Topic.OPENED_READED_STATE;
            if (el != null) {
                String modifier = el.text();
                if ("+".equals(modifier))//есть новые ответы
                    state = Topic.OPENED_UNREADED_STATE;
                if ("-".equals(modifier))//есть новые ответы
                    state = Topic.OPENED_READED_STATE;
                if ("X".equals(modifier))//есть новые ответы
                    state = Topic.CLOSED_READED_STATE;
            }
            el = element.select("span.topic_desc").first();
            if (el != null) {
                String text = el.firstElementSibling().text();
                Matcher m = descriptionPattern.matcher(text);
                if (m.find())
                    topic.setDescription(m.group(1));
                Boolean hasUnread = el.html().contains("view=getnewpost");
                if (hasUnread)
                    state = state == Topic.CLOSED_READED_STATE ? Topic.CLOSED_UNREADED_STATE : Topic.OPENED_UNREADED_STATE;
            }
            topic.setState(state);
            el = element.select("span.topic_desc a[href~=showforum=\\d+]").first();
            if (el != null) {
                Uri uri = Uri.parse(el.attr("href"));
                String id = uri.getQueryParameter("showforum");
                String title = el.text();
                topic.setForumId(id);
                topic.setForumTitle(title);
            }
            el = element.select("div.topic_body>a[href~=showuser=\\d+]").first();
            if (el != null) {
                Uri uri = Uri.parse(el.attr("href"));
                String id = uri.getQueryParameter("showuser");
                String nick = el.text();
                topic.setLastMessageAuthor(nick);
                topic.setLastMessageDate(Functions.parseForumDateTime(el.nextSibling().toString(), today, yesterday));
            }
            res.add(topic);
        }
        Element el = doc.select("div.pagination").first();
        if (el != null) {
            Pattern pagesCountPattern = Pattern.compile("<a href=\"/forum/index.php.*?st=(\\d+)\">", Pattern.CASE_INSENSITIVE);


            Matcher m = pagesCountPattern.matcher(el.html());
            int themesCount = 0;
            while (m.find()) {
                themesCount = Math.max(Integer.parseInt(m.group(1)) + 1, themesCount);
            }
            listInfo.setOutCount(themesCount);
        }
        return res;
    }

}
