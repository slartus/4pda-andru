package org.softeg.slartus.forpdaapi;

import android.text.Html;
import android.text.TextUtils;

import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.softeg.slartus.forpdaapi.classes.LoginForm;
import org.softeg.slartus.forpdacommon.Http;
import org.softeg.slartus.forpdacommon.NotReportException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/*
 * Created by slinkin on 07.02.14.
 */
public class ProfileApi {
    /**
     * Проверка логина на странице
     */
    public static void checkLogin(String pageBody, LoginResult loginResult) {
        Matcher kMatcher = Pattern.compile("action=logout[^\\\"]*[;&]k=([^&\\\"]*)", Pattern.CASE_INSENSITIVE)
                .matcher(pageBody);

        Matcher userPattern=Pattern.compile("\"[^\"]*showuser=(\\d+)[^>]*>Профиль<\\/a>", Pattern.CASE_INSENSITIVE).matcher(pageBody);

        if (kMatcher.find()&&userPattern.find()) {
            loginResult.setUserId(userPattern.group(1));
            loginResult.setK(kMatcher.group(1));
            loginResult.setSuccess(true);

            String[] avatarPatterns = {"(?:'|\")([^'\"]*4pda.(?:to|ru)/*?forum/*?uploads/*?av-[^?'\"]*)",
                    "(?:'|\")([^'\"]*4pda.(?:to|ru)/*?forum/*?style_avatars/[^?'\"]*)"};
            for (String avatarPattern : avatarPatterns) {
                Matcher m = Pattern.compile(avatarPattern, Pattern.CASE_INSENSITIVE).matcher(pageBody);
                if (m.find()) {
                    loginResult.setUserAvatarUrl(m.group(1));
                    break;
                }
            }
        }
    }

    public static String getUserNick(IHttpClient httpClient, CharSequence userID) throws IOException {
        return Jsoup.parse(httpClient.performGet("https://"+ Http.Host+"/forum/index.php?showuser=" + userID)).select("div.user-box > h1").first().text();
    }

    /**
     * @throws Exception
     */
    public static LoginResult login(IHttpClient httpClient, String login, String password,
                                    Boolean privacy, String capVal, String capTime, String capSig) throws Exception {
        LoginResult loginResult = new LoginResult();

        Map<String, String> additionalHeaders = new HashMap<>();

        additionalHeaders.put("login", login);
        additionalHeaders.put("password", password);
        //additionalHeaders.put("CookieDate", "1");
        if (privacy)
            additionalHeaders.put("hidden", "1");
        additionalHeaders.put("act", "auth");
        //additionalHeaders.put("CODE", "01");
        additionalHeaders.put("referer", "https://"+Http.Host+"/forum/index.php");
        //additionalHeaders.put("s", session);
        additionalHeaders.put("captcha", capVal);
        additionalHeaders.put("captcha-time", capTime);
        additionalHeaders.put("captcha-sig", capSig);
        additionalHeaders.put("return", "https://"+Http.Host+"/forum/index.php");


        String res = httpClient.performPost("https://"+Http.Host+"/forum/index.php?act=auth", additionalHeaders);

        if (TextUtils.isEmpty(res)) {
            loginResult.setLoginError("Сервер вернул пустую страницу");
            return loginResult;
        }

        for (Cookie cookie : httpClient.getCookieStore().getCookies()) {
            if ("member_id".equals(cookie.getName())) {
                // id пользователя. если он есть - логин успешный
                loginResult.setUserId(cookie.getValue());
                loginResult.setUserLogin(cookie.getValue());
                loginResult.setSuccess(true);
                break;
            } else if ("pass_hash".equals(cookie.getName())) {
                // хэш пароля
            } else if ("session_id".equals(cookie.getName())) {
                // id сессии
            }
        }

        checkLogin(res, loginResult);

        if (!loginResult.isSuccess()) {
            loginResult.setLoginError("Неизвестная ошибка");

            Pattern checkPattern = Pattern.compile("\t\t<h4>Причина:</h4>\n" +
                    "\n" +
                    "\t\t<p>(.*?)</p>", Pattern.MULTILINE);
            Matcher m = checkPattern.matcher(res);
            if (m.find()) {
                loginResult.setLoginError(m.group(1));
            } else {
                checkPattern = Pattern.compile("<ul[\\s\\S]*?<li>([\\s\\S]*?)</li>");
                m = checkPattern.matcher(res);
                if (m.find()) {
                    loginResult.setLoginError(m.group(1));
                } else {
                    loginResult.setLoginError(Html.fromHtml(res).toString());
                }
            }
        }

        return loginResult;
    }

    /**
     * ЛОгаут
     *
     * @param k идентификатор, полученный при логине
     */
    public static String logout(IHttpClient httpClient, String k) throws IOException {
        httpClient.deleteFileCookies();
        return httpClient.performGet("https://"+Http.Host+"/forum/index.php?act=Login&CODE=03&k=" + k);
    }

    public static Profile getProfile(IHttpClient httpClient, CharSequence userID) throws IOException {
        Profile profile = new Profile();
        profile.setId(userID);
        String page = httpClient.performGet("https://"+Http.Host+"/forum/index.php?showuser=" + userID);
        page = page.replace("\"//", "\"https://");
        Document doc = Jsoup.parse(page);
        org.jsoup.nodes.Element element = doc.select("li:has(h2:contains(Блокнот))").first();
        if (element != null) {
            org.jsoup.nodes.Element el = element.select("textarea.profile-textarea").first();
            if (el != null)
                el.attr("name", "profile_notepad");
            el = element.select("a.btn").first();
            if (el != null) {
                el.removeAttr("href");
                el.attr("onclick", "saveProfileNotepad();");
            }
        }

        element = doc.select("div.user-box").first();
        if (element == null) {
            profile.setHtmlBody(doc.html());
        } else {
            profile.setHtmlBody("<ul class=\"user-profile-list\">" + element.parent().parent().html() + "</ul>");
        }

        assert element != null;
        org.jsoup.nodes.Element userNickElement = element.select("div.user-box > h1").first();
        if (userNickElement != null)
            profile.setNick(userNickElement.text());
        return profile;
    }

    private static String RequestUrl(OkHttpClient client, String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public static LoginForm getLoginForm() throws IOException {
        OkHttpClient client=new OkHttpClient.Builder()
                .retryOnConnectionFailure(true)
                .connectTimeout(15, TimeUnit.SECONDS) // connect timeout
                .writeTimeout(15, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS).build();

        String prevPage = RequestUrl(client,"https://"+Http.Host+"/forum/index.php?act=auth");
        Matcher m = Pattern.compile("act=auth[^\"]*[;&]k=([^&\"]*)", Pattern.CASE_INSENSITIVE).matcher(prevPage);
        String k = UUID.randomUUID().toString();
        if (m.find())
            k = m.group(1);
        String page =  RequestUrl(client,"https://"+Http.Host+"/forum/index.php?act=auth&k=" + k);

        m = Pattern
                .compile("<form[^>]*?>([\\s\\S]*?)</form>")
                .matcher(page);
        if (!m.find())
            throw new NotReportException("Форма логина не найдена");
        String formText = m.group(1);
        m = Pattern
                .compile("<img[^>]*?src=\"([^\"]*?captcha[^\"]*)\"")
                .matcher(formText);
        if (!m.find())
            throw new NotReportException("Капча не найдена");

        LoginForm loginForm = new LoginForm();
        loginForm.setCapPath(m.group(1));

        m = Pattern
                .compile("name=\"captcha-time\"[^>]*?value=\"([^\"]*)\"")
                .matcher(formText);
        if (!m.find())
            throw new NotReportException("cap_time не найден");
        loginForm.setCapTime(m.group(1));
        m = Pattern
                .compile("name=\"captcha-sig\"[^>]*?value=\"([^\"]*)\"")
                .matcher(formText);
        if (!m.find())
            throw new NotReportException("cap_sig не найден");
        loginForm.setCapSig(m.group(1));

        m = Pattern
                .compile("name=\"s\"[^>]*?value=\"([^\"]*)\"")
                .matcher(formText);
        if (m.find())
            loginForm.setSession(m.group(1));

        return loginForm;
    }


}
