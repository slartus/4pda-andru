package org.softeg.slartus.forpdaapi.post;

import android.view.View;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by slinkin on 15.09.2016.
 */
public class EditAttachesList implements Serializable {
    private List<EditAttach> attaches=new ArrayList<>();

    public List<EditAttach> getAttaches() {
        return attaches;
    }

    public int size() {
        return attaches.size();
    }

    public EditAttach get(int which) {
        return attaches.get(which);
    }

    public void add(EditAttach editAttach) {
        attaches.add(editAttach);
    }
}
