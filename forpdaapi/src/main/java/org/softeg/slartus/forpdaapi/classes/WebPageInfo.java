package org.softeg.slartus.forpdaapi.classes;/*
 * Created by slinkin on 18.11.2015.
 */

public class WebPageInfo {
    private Throwable error;
    private String page;
    private String title;
    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
    }


    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
