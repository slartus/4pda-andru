package org.softeg.slartus.forpdaapi;

import android.net.Uri;
import android.text.Html;
import android.text.TextUtils;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.softeg.slartus.forpdaapi.search.SearchApi;
import org.softeg.slartus.forpdacommon.Functions;
import org.softeg.slartus.forpdacommon.Http;
import org.softeg.slartus.forpdacommon.HttpHelper;
import org.softeg.slartus.forpdacommon.NotReportException;
import org.softeg.slartus.forpdacommon.PatternExtensions;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * Created by slinkin on 20.02.14.
 */
public class TopicsApi {
    final static Pattern countPattern = PatternExtensions.compile("<a href=\"/forum/index.php\\?act=[^\"]*?st=(\\d+)\">&raquo;</a>");
    final static Pattern mainPattern = PatternExtensions.compile("cat_name[\\s\\S]*?</div>([\\s\\S]*<br />)<div class=\"forum_mod_funcs\">");
    final static Pattern topicsPattern = PatternExtensions.compile("(<div data-item-fid[\\s\\S]*?</script></div></div>)");
    final static Pattern topicPattern = PatternExtensions.compile("<div data-item-fid=\"(\\d*)\" data-item-track=\"(\\w*)\" data-item-pin=\"(\\d)\"[\\s\\S]*?<a href=\"([^\"]*?)\"[^>]*>(<strong>|)([\\s\\S]*?)(</strong>|)</a>[\\s\\S]*?<span class=\"topic_desc\">([\\s\\S]*?)(<[\\s\\S]*?showforum=(\\d*?)\"[\\s\\S]*?)<a href=\"[^\"]*view=getlastpost[^\"]*\">Послед.:</a>\\s*<a href=\"/forum/index.php\\?showuser=\\d+\">(.*?)</a>(.*?)<");
    public static ArrayList<FavTopic> getFavTopics(IHttpClient client,
                                                   ListInfo listInfo) throws ParseException, IOException, URISyntaxException {
        return getFavTopics(client, null, null, null, null, false, false, listInfo);
    }

    public static ArrayList<FavTopic> getFavTopics(IHttpClient client,
                                                   String sortKey,
                                                   String sortBy,
                                                   String pruneDay,
                                                   String topicFilter,
                                                   Boolean unreadInTop,
                                                   Boolean fullPagesList,
                                                   ListInfo listInfo) throws ParseException, IOException, URISyntaxException {
        List<NameValuePair> qparams = new ArrayList<NameValuePair>();
        qparams.add(new BasicNameValuePair("act", "fav"));
        qparams.add(new BasicNameValuePair("type", "topics"));
        if (sortKey != null)
            qparams.add(new BasicNameValuePair("sort_key", sortKey));
        if (sortBy != null)
            qparams.add(new BasicNameValuePair("sort_by", sortBy));
        if (pruneDay != null)
            qparams.add(new BasicNameValuePair("prune_day", pruneDay));
        if (topicFilter != null)
            qparams.add(new BasicNameValuePair("topicfilter", topicFilter));
        qparams.add(new BasicNameValuePair("st", Integer.toString(listInfo.getFrom())));


        URI uri = URIUtils.createURI("https", Http.Host, -1, "/forum/index.php",
                URLEncodedUtils.format(qparams, "UTF-8"), null);

        String pageBody = client.performGet(uri.toString());

        Document doc = Jsoup.parse(pageBody);

        Matcher m = PatternExtensions.compile("<a href=\"/forum/index.php\\?act=[^\"]*?st=(\\d+)\">&raquo;</a>").matcher(pageBody);
        if (m.find()) {
            listInfo.setOutCount(Integer.parseInt(m.group(1)) + 1);
        }

        Pattern lastPostPattern = Pattern.compile("<a href=\"[^\"]*view=getlastpost[^\"]*\">Послед.:<\\/a>\\s*<a href=\"[^\"]*showuser=\\d+\">(.*?)<\\/a>(.*)(?:$|<)",
                Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        Pattern trackTypePattern = Pattern.compile("wr_fav_subscribe\\(\\d+,\"(\\w+)\"\\);",
                Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
        ArrayList<FavTopic> res = new ArrayList<>();
        String today = Functions.getToday();
        String yesterday = Functions.getYesterToday();

        for (Element topicElement : doc.select("div[data-item-fid]")) {
            Elements elements = topicElement.select("div.topic_title");
            if (elements.size() == 0) continue;
            Element topicTitleDivElement = elements.first();
            elements = topicTitleDivElement.select("a");
            if (elements.size() == 0) continue;
            Element element = elements.first();
            Uri ur = Uri.parse(element.attr("href"));
            String tId = topicElement.attr("data-item-fid");
            Boolean pinned = "1".equals(topicElement.attr("data-item-pin"));
            String trackType = null;
            elements = topicElement.select("div.topic_body");
            if (elements.size() > 0) {
                String html = elements.first().html();
                m = trackTypePattern.matcher(html);
                if (m.find())
                    trackType = m.group(1);
            }


            if (TextUtils.isEmpty(ur.getQueryParameter("showtopic"))) {
                FavTopic topic = new FavTopic(null, topicTitleDivElement.text());
                topic.setTid(tId);
                topic.setPinned(pinned);
                topic.setTrackType(trackType);
                topic.setDescription("Форум");
                //topic.setSortOrder(Integer.toString(sortOrder++));
                res.add(topic);
                continue;
            }


            String id = ur.getQueryParameter("showtopic");
            String title = element.text();
            FavTopic topic = new FavTopic(id, title);
            topic.setTid(tId);
            topic.setTrackType(trackType);
            int state = Topic.OPENED_READED_STATE;


            Element el = topicElement.select("span.modifier").first();
            if (el != null) {
                String modifier = el.text();
                if ("+".equals(modifier))//есть новые ответы
                    state =Topic.OPENED_UNREADED_STATE;
                if ("-".equals(modifier))//есть новые ответы
                    state =Topic.OPENED_READED_STATE;
                if ("X".equals(modifier))//есть новые ответы
                    state =Topic.CLOSED_READED_STATE;
            }

            elements = topicElement.select("div.topic_body");
            if (elements.size() > 0) {
                Element topicBodyDivElement = elements.first();
                elements = topicBodyDivElement.select("span.topic_desc");
                if (elements.size() > 0)
                    topic.setDescription(elements.first().text());
                String text = topicBodyDivElement.html();
                Boolean hasUnread = text.contains("view=getnewpost");
                if (hasUnread)
                    state = state == Topic.CLOSED_READED_STATE ? Topic.CLOSED_UNREADED_STATE : Topic.OPENED_UNREADED_STATE;
                topic.setState(state);

                m = lastPostPattern.matcher(text);
                if (m.find()) {
                    topic.setLastMessageDate(Functions.parseForumDateTime(m.group(2), today, yesterday));
                    topic.setLastMessageAuthor(m.group(1));
                }
                //topic.setSortOrder(Integer.toString(sortOrder++));
                res.add(topic);
            }
        }
        if (fullPagesList) {
            while (true) {
                if (listInfo.getOutCount() <= res.size())
                    break;
                listInfo.setFrom(res.size());
                ArrayList<FavTopic> nextPageTopics = getFavTopics(client, sortKey, sortBy, pruneDay, topicFilter, false, false, listInfo);
                if (nextPageTopics.size() == 0)
                    break;
                res.addAll(nextPageTopics);
            }
        }
        if (unreadInTop) {
            final int asc = -1;// новые вверху
            Collections.sort(res, new Comparator<Topic>() {
                @Override
                public int compare(Topic topic, Topic topic2) {
                    if (topic.getState() == topic2.getState())
                        return 0;
                    return (topic.getState() == Topic.FLAG_NEW) ? asc : (-asc);

                }
            });
        }
        if (res.size() == 0) {
            m = PatternExtensions.compile("<div class=\"errorwrap\">([\\s\\S]*?)</div>")
                    .matcher(pageBody);
            if (m.find()) {
                throw new NotReportException(Html.fromHtml(m.group(1)).toString(), new Exception(Html.fromHtml(m.group(1)).toString()));
            }
        }
        return res;
    }

    public static ArrayList<Topic> getForumTopics(IHttpClient client,
                                                  String url,
                                                  String forumId,

                                                  Boolean unreadInTop,
                                                  ListInfo listInfo) throws ParseException, IOException, URISyntaxException {

        String pageBody = client.performGet(url);

        ArrayList<Topic> res = new ArrayList<Topic>();

        if ((HttpHelper.getRedirectUri() != null && HttpHelper.getRedirectUri().toString().toLowerCase().contains("act=search"))
                || url.toLowerCase().contains("act=search")) {
            res = SearchApi.parse(pageBody, listInfo);
        } else {
            Document doc = Jsoup.parse(pageBody, "https://"+ Http.Host+"");
            Elements postsElements = doc.select("div[data-topic]");
            Pattern descriptionPattern = Pattern.compile("^(?!(?:автор|форум):)([^<]*)(?:$|<)", Pattern.CASE_INSENSITIVE);

            String today = Functions.getToday();
            String yesterday = Functions.getYesterToday();
            for (Element element : postsElements) {
                Element el = element.select("a[href~=showtopic=\\d+]").first();

                Topic topic;
                if (el != null) {
                    Uri uri = Uri.parse(el.attr("href"));
                    String id = uri.getQueryParameter("showtopic");
                    String title = el.text();
                    topic = new Topic(id, title);
                } else
                    continue;

                el = element.select("span.modifier").first();
                int state = Topic.OPENED_READED_STATE;
                if (el != null) {
                    String modifier = el.text();
                    if ("+".equals(modifier))//есть новые ответы
                        state = Topic.OPENED_UNREADED_STATE;
                    if ("-".equals(modifier))//есть новые ответы
                        state = Topic.OPENED_READED_STATE;
                    if ("X".equals(modifier))//есть новые ответы
                        state = Topic.CLOSED_READED_STATE;
                }

                el = element.select("span.topic_desc").first();
                if (el != null) {
                    String text = el.firstElementSibling().html();
                    Matcher m = descriptionPattern.matcher(text);
                    if (m.find())
                        topic.setDescription(m.group(1));
                    Boolean hasUnread = el.html().contains("view=getnewpost");
                    if (hasUnread)
                        state = state == Topic.CLOSED_READED_STATE ? Topic.CLOSED_UNREADED_STATE : Topic.OPENED_UNREADED_STATE;
                }
                topic.setState(state);

                el = element.select("span.topic_desc a[href~=showforum=\\d+]").first();
                if (el != null) {
                    Uri uri = Uri.parse(el.attr("href"));
                    String id = uri.getQueryParameter("showforum");
                    String title = el.text();
                    topic.setForumId(id);
                    topic.setForumTitle(title);
                }
                el = element.select("div.topic_body>a[href~=showuser=\\d+]").first();
                if (el != null) {
                    Uri uri = Uri.parse(el.attr("href"));
                    String id = uri.getQueryParameter("showuser");
                    String nick = el.text();
                    topic.setLastMessageAuthor(nick);
                    topic.setLastMessageDate(Functions.parseForumDateTime(el.nextSibling().toString(), today, yesterday));
                }
                res.add(topic);
            }
            Element el = doc.select("div.pagination").first();
            if (el != null) {
                Pattern pagesCountPattern = Pattern.compile("<a href=\"/forum/index.php.*?st=(\\d+)\">", Pattern.CASE_INSENSITIVE);


                Matcher m = pagesCountPattern.matcher(el.html());
                int themesCount = 0;
                while (m.find()) {
                    themesCount = Math.max(Integer.parseInt(m.group(1)) + 1, themesCount);
                }
                listInfo.setOutCount(themesCount);
            }
        }
        if (unreadInTop) {
            final int asc = -1;// новые вверху
            Collections.sort(res, new Comparator<Topic>() {
                @Override
                public int compare(Topic topic, Topic topic2) {
                    if (topic.getState() == topic2.getState())
                        return 0;
                    return (topic.getState() == Topic.FLAG_NEW) ? asc : (-asc);

                }
            });
        }
        if (res.size() == 0) {
            Matcher m = PatternExtensions.compile("<div class=\"errorwrap\">([\\s\\S]*?)</div>")
                    .matcher(pageBody);
            if (m.find()) {
                throw new NotReportException(Html.fromHtml(m.group(1)).toString(), new Exception(Html.fromHtml(m.group(1)).toString()));
            }
        }
        return res;
    }

}
